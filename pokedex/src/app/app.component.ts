import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'po';
  mode = 'side'
  opened = true;
  layoutGap = '64';
  fixedInViewport = true;
}
