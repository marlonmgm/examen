import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//Componentes material design
import {MatSidenavModule} from '@angular/material/sidenav';
import { MatButtonModule,MatExpansionModule, MatListModule, MatCheckboxModule, MatDialogModule, MatIconModule, MatToolbarModule, MatDividerModule, MatTooltipModule, MatSelectModule,MatChipsModule,MatTabsModule,MatRadioModule } from '@angular/material';

//ruta de cada componente
import { RoutingModule } from './routing/routing.module';


//servicios de componentes

import { HomeService } from './home/home.service';
import { MessageService } from './message.service';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatListModule,
    RoutingModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
  ],
  providers : [
    HomeService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
