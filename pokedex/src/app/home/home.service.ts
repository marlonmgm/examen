import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Pokemon } from './../types';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from './../message.service';
import * as _ from 'lodash';

@Injectable()
export class HomeService {
  
  private pokemonurl = environment.apiUrl + '/pokemon';  // URL to web api
  private evolucionurl = environment.apiUrl + '/pokemondetalle';  // URL to web api
  private historialurl = environment.apiUrl + '/historial';  // URL to web api

  constructor(private http: HttpClient, private messageService: MessageService) { }

  list(params?: any): Observable<any> {
    let url = `${this.pokemonurl}?limit=16`;
    if (params) {
      _.mapKeys(params, (value, key) => {
        url += key + '=' + value + '&';
      });
    }
    return this.http.get<any[]>(url)
    .pipe(
      tap(pokemon => this.log(`lista de pokemons`)),
      catchError(this.handleError('HomeService.list', []))
    );
  }
  get(id: any): Observable<any> {
    let url = `${this.pokemonurl}/${id}?`;
    return this.http.get<any[]>(url)
    .pipe(
      tap(pokemon => this.log(`lista de pokemons`)),
      catchError(this.handleError('HomeService.list', []))
    );
  }
  evoluciones(id: any): Observable<any> {
    let url = `${this.evolucionurl}/${id}?`;
    return this.http.get<any[]>(url)
    .pipe(
      tap(pokemon => this.log(`lista de pokemons`)),
      catchError(this.handleError('HomeService.list', []))
    );
  }

  save(data : any): Observable<any> {
    const url = `${this.historialurl}`;
    return this.http.post<any>(url, data)
      .pipe(
        tap((data: any) => this.log(`Historial guardado id=${data.id}`)),
        catchError(this.handleError<any>('HomeService.save'))
      );
  }

  historial(params?: any): Observable<any> {
    let url = `${this.historialurl}?limit=5`;
    if (params) {
      _.mapKeys(params, (value, key) => {
        url += key + '=' + value + '&';
      });
    }
    return this.http.get<any[]>(url)
    .pipe(
      tap(pokemon => this.log(`lista de historial`)),
      catchError(this.handleError('HomeService.list', []))
    );
  }



  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add('UsuariosService: ' + message);
  }

}
