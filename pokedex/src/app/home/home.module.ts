import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';

//componentes material design
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from "@angular/flex-layout";
import { MatInputModule } from '@angular/material/input';
import  { FormsModule } from '@angular/forms';

//libreria para buscar
import { Ng2SearchPipeModule } from 'ng2-search-filter';
const routes = [
    {
        path: '',
        component: HomeComponent
    }
];

@NgModule({
    declarations: [
        HomeComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        MatCardModule,
        MatButtonModule,
        CommonModule,
        FlexLayoutModule,
        MatInputModule,
        FormsModule,
        Ng2SearchPipeModule
    ],
    exports: [
        HomeComponent
    ]
})

export class HomeModule {
}