import { Component } from '@angular/core';
import { HomeService } from './home.service';
import { Pokemon } from '../types';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  title = 'po';

  pokemon : Pokemon[];
  searchText;

  constructor(private servicio : HomeService){
    this.servicio.list()
    .subscribe(registros => {
      this.pokemon = registros;
      console.log(this.pokemon);
    });

  }

}
