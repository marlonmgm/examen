import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DetalleComponent } from './detalle.component';

//componentes material design
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from "@angular/flex-layout";
import {MatTabsModule} from '@angular/material/tabs';
import {MatListModule} from '@angular/material/list';
import {MatChipsModule} from '@angular/material/chips';
const routes = [
    {
        path: '',
        component: DetalleComponent
    }
];

@NgModule({
    declarations: [
        DetalleComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        MatCardModule,
        MatButtonModule,
        CommonModule,
        FlexLayoutModule,
        MatTabsModule,
        MatListModule,
        MatChipsModule
    ],
    exports: [
        DetalleComponent
    ]
})

export class DetalleModule {
}