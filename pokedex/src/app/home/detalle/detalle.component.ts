import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HomeService } from '../home.service';
import { Pokemon } from '../../types';
@Component({
    selector: 'app-home-detalle',
    templateUrl: './detalle.component.html',
    styleUrls: ['./detalle.component.scss']
})
export class DetalleComponent implements OnInit {
    title = 'po';

    pokemon;

    pokemonID;
    evoluciones;
    total;
    evolucion;

    constructor(
        private servicio: HomeService,
        private route: ActivatedRoute,
        private router: Router,
    ) {
        //obtener id de url
        this.pokemonID = this.route.snapshot.paramMap.get('id');

    }

    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        //si tenemos id llamamos servicio con informacion de pokemon
        if (this.pokemonID) {
            this.servicio.get(this.pokemonID)
                .subscribe(registros => {
                    this.pokemon = registros;
                    console.log(this.pokemon);
                    this.total = Number(Number(this.pokemon.detalle[0].stats.speed) + Number(this.pokemon.detalle[0].stats.hp) + Number(this.pokemon.detalle[0].stats.attack) + Number(this.pokemon.detalle[0].stats.defense) + Number(this.pokemon.detalle[0].stats.specialattack) + Number(this.pokemon.detalle[0].stats.specialdefense));
                    this.evolucion = Object.values(this.pokemon.detalle[0].evolution);
                    console.log(this.evolucion);
                    let data = { "name": this.pokemon.name };
                    console.log(data);
                    this.servicio.save(data)
                        .subscribe(res => {
                            console.log(res);
                        });
                });

        }
    }

}