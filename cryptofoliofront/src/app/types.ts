export class Crypto {
    id: string;
    name: string;
    symbol: string;
    balance: string;
    rateUSD: string;
    url: string;
}