import { Injectable } from '@angular/core';

import { environment } from './../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Crypto } from './types';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from './message.service';
import * as _ from 'lodash';

@Injectable()
export class AppService {
  
  private cryptourl = environment.apiUrl + '/crypto';  // URL to web api

  constructor(private http: HttpClient, private messageService: MessageService) { }

  list(params?: any): Observable<any> {
    let url = `${this.cryptourl}?`;
    if (params) {
      _.mapKeys(params, (value, key) => {
        url += key + '=' + value + '&';
      });
    }
    return this.http.get<any[]>(url)
    .pipe(
      tap(pokemon => this.log(`lista de monedas`)),
      catchError(this.handleError('AppService.list', []))
    );
  }

  get(id: any): Observable<any> {
    let url = `${this.cryptourl}/${id}?`;
    return this.http.get<any[]>(url)
    .pipe(
      tap(pokemon => this.log(`lista de pokemons`)),
      catchError(this.handleError('HomeService.list', []))
    );
  }

  update(data: Crypto): Observable<Crypto> {
    const url = `${this.cryptourl}/${data.id}`;
    let registro = _.omit(data, ['usuarioCreacion', 'propietario', 'group']);
    return this.http.patch<Crypto>(url, registro)
      .pipe(
        tap((data: Crypto) => this.log(`crypto actualizdo id=${data.id}`)),
        catchError(this.handleError<Crypto>('AppService.update'))
      );
  }

  /*transferencia(data : Crypto): Observable<Crypto> {
    const url = `${this.historialurl}`;
    return this.http.post<any>(url, data)
      .pipe(
        tap((data: any) => this.log(`Historial guardado id=${data.id}`)),
        catchError(this.handleError<any>('HomeService.save'))
      );
  }*/



  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add('UsuariosService: ' + message);
  }

}
