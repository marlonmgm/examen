import { Component } from '@angular/core';


//servicio
import { AppService } from './app.service';


//types
import { Crypto } from './types';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  money;
  //valores de las primeras cajas de texto
  valor1;
  valor2;
  valor3;
  valor4;
  valor5;

  //total en usd
  total;
  //monedas y valores de select
  monedas : Crypto[];


  //cuentas
  acc1;
  acc2;

  //resta y suma en la transferencia
  rtransfer;
  stransfer;

  updatevalores1;
  updatevalores2;



  constructor(private service : AppService){
    /*this.valor1 = this.monedas[0].balance;
    this.valor2 = this.monedas[1].balance;
    this.valor3 = this.monedas[2].balance;
    this.valor4 = this.monedas[3].balance;*/

    this.carga();
  }

  carga(){
    this.service.list()
    .subscribe(registros => {
      this.monedas = registros;
      console.log(this.monedas)
      if(this.monedas[0].symbol == "BTC"){
        this.valor1 = this.monedas[0].balance;
      }
      if(this.monedas[1].symbol == "ETH"){
        this.valor2 = this.monedas[1].balance;
      }
      if(this.monedas[2].symbol == "LTD"){
        this.valor3 = this.monedas[2].balance;
      }
      if(this.monedas[3].symbol == "PRG"){
        this.valor4 = this.monedas[3].balance;
      }
      let t = [];
      this.monedas.forEach(item => {
        let a = (parseFloat(item.balance) * parseFloat(item.rateUSD));
        t.push(a);
      });
      this.total = t.reduce((a,b) => a+b,0);
    });
  }

  monedaLink(link){
    console.log("moneda")
    console.log(link);
    window.open(link, "_blank");
  }

  select1(res){
    console.log(res);
    if(res.value =="BTC")
    {
      this.valor1 = this.monedas[0].balance;
    }

    if(res.value =="ETH")
    {
      this.valor1 = this.monedas[1].balance;
    }

    if(res.value =="LTD")
    {
      this.valor1 = this.monedas[2].balance;
    }

    if(res.value =="PRG")
    {
      this.valor1 = this.monedas[3].balance;
    }
  }
  select2(res){
    if(res.value =="BTC")
    {
      this.valor1 = this.monedas[0].balance;
    }

    if(res.value =="ETH")
    {
      this.valor1 = this.monedas[1].balance;
    }

    if(res.value =="LTD")
    {
      this.valor1 = this.monedas[2].balance;
    }

    if(res.value =="PRG")
    {
      this.valor1 = this.monedas[3].balance;
    }
  }
  select3(res){
    if(res.value =="BTC")
    {
      this.valor1 = this.monedas[0].balance;
    }

    if(res.value =="ETH")
    {
      this.valor1 = this.monedas[1].balance;
    }

    if(res.value =="LTD")
    {
      this.valor1 = this.monedas[2].balance;
    }

    if(res.value =="PRG")
    {
      this.valor1 = this.monedas[3].balance;
    }
  }
  select4(res){
    if(res.value =="BTC")
    {
      this.valor1 = this.monedas[0].balance;
    }

    if(res.value =="ETH")
    {
      this.valor1 = this.monedas[1].balance;
    }

    if(res.value =="LTD")
    {
      this.valor1 = this.monedas[2].balance;
    }

    if(res.value =="PRG")
    {
      this.valor1 = this.monedas[3].balance;
    }
  }

  select5(res){
    console.log(res);
  this.acc1 = res.value;
  this.service.get(res.value)
    .subscribe(registros => {
      console.log("select5")
      console.log(registros);
      this.rtransfer = Number(Number(registros.balance) - Number(this.valor5));
    });
  }
  select6(res){
    console.log(res);
    this.acc2 = res.value;
    this.service.get(res.value)
    .subscribe(registros => {
      console.log("select6")
      console.log(registros);
      this.stransfer = Number(Number(registros.balance) + Number(this.valor5));
    });
  }

  transfer(a){
    console.log(a)
    if(this.acc2 && this.acc1){
      console.log(this.rtransfer);
      console.log(this.stransfer);

      this.updatevalores1 = {id : this.acc1, balance : this.rtransfer};
      this.updatevalores2 = {id : this.acc2, balance : this.stransfer};
      console.log(this.updatevalores1);
      this.service.update(this.updatevalores1)
      .subscribe(res => {
      });
      this.service.update(this.updatevalores2)
      .subscribe(res => {
        this.carga();
      });
    }
  }
}
